package loggerx

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestInitLogger(t *testing.T) {
	t.Run("Test logger initialization", func(t *testing.T) {

		logger := InitLogger("devLogger", false)

		assert.NotNil(t, logger)
		assert.Equal(t, logger.Core().Enabled(zap.DebugLevel), true, "incorrect logger level")

	})

	t.Run("Test production mode logger initialization", func(t *testing.T) {

		logger := InitLogger("prodLogger", true)

		assert.NotNil(t, logger)
		assert.Equal(t, logger.Core().Enabled(zap.InfoLevel), true, "incorrect logger level")
		assert.Equal(t, logger.Core().Enabled(zap.DebugLevel), false, "incorrect logger level")
		
	})

}
