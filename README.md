# Logger
Простой и настраиваемый логгер, построенный на базе библиотеки логирования Zap. Он предоставляет возможность инициализации логгера с настраиваемым режимом продакшн.

## Установка

```bash
go get "gitlab.com/golight/loggerx"
```

## Доступные функции и методы
```go
//Инициализирует и возвращает новый экземпляр логгера. Принимает имя логгера и флаг продакшн для настройки.
    func InitLogger(name string, production bool) *zap.Logger
```

## Уровни логирования
Логгер поддерживает следующие уровни логирования в порядке увеличения серьезности:

- Debug 
- Info
- Warn
- Error
- DPanic (паника на уровне отладки)
- Panic (паника на любом уровне)
- Fatal (завершение после логирования)

## Пример использования
```go
package main

import (
	"gitlab.com/golight/loggerx"
)

func main() {
	logger := loggerx.InitLogger("myApp", true)

	logger.Info("Application started")
	logger.Warn("Something might be wrong")
	logger.Error("An error occurred")

}
```