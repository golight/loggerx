package main

import (
	"gitlab.com/golight/loggerx"
)

func main() {
	logger := loggerx.InitLogger("myApp", true)

	logger.Info("Application started")
	logger.Warn("Something might be wrong")
	logger.Error("An error occurred")

}